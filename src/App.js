import './App.css';
import Chat from './components/Chat/Chat';

function App() {
  const chatData =
    'https://edikdolynskyi.github.io/react_sources/messages.json';

  return (
    <div className="App">
      <Chat url={chatData} />
    </div>
  );
}

export default App;
