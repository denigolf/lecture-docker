import {
  LOAD_MESSAGES,
  TOGGLE_PRELOADER,
  TOGGLE_EDIT_MODAL,
  DELETE_MESSAGE,
  SEND_MESSAGE,
  EDIT_MESSAGE,
  SET_CURRENT_MESSAGE_ID,
  DROP_CURRENT_MESSAGE_ID,
} from '../actions/actionTypes';

import { v4 as uuidv4 } from 'uuid';

const defaultState = {
  messages: [],
  editModal: false,
  preloader: true,
  currentMessageId: '',
};

const chat = (state = defaultState, action) => {
  switch (action.type) {
    case LOAD_MESSAGES: {
      const { messages } = action.payload;
      return { ...state, messages };
    }
    case TOGGLE_PRELOADER: {
      return { ...state, preloader: !state.preloader };
    }
    case TOGGLE_EDIT_MODAL: {
      return { ...state, editModal: !state.editModal };
    }
    case SET_CURRENT_MESSAGE_ID: {
      const { messageId } = action.payload;
      return { ...state, currentMessageId: messageId };
    }
    case DROP_CURRENT_MESSAGE_ID:
      return { ...state, currentMessageId: '' };
    case SEND_MESSAGE: {
      const { text, clearInput } = action.payload;
      if (!text.trim()) {
        clearInput();
        return state;
      }
      const messages = [...state.messages];
      const userId = 'my-id';
      let currentTime = new Date();
      currentTime = currentTime.toJSON();
      messages.push({
        id: uuidv4(),
        userId: userId,
        text: text,
        own: true,
        createdAt: currentTime,
      });
      clearInput();
      return { ...state, messages };
    }
    case EDIT_MESSAGE: {
      const { messageId, text } = action.payload;
      if (text.trim() === '') {
        return state;
      }
      const messages = [...state.messages];
      const index = messages.findIndex((message) => {
        return message.id === messageId;
      });
      messages[index] = { ...messages[index], text };
      return { ...state, messages, editModal: true };
    }
    case DELETE_MESSAGE: {
      const { id } = action.payload;
      const messages = [...state.messages];
      const index = messages.findIndex((message) => {
        return message.id === id;
      });
      messages.splice(index, 1);
      return { ...state, messages };
    }
    default:
      return state;
  }
};

export default chat;
