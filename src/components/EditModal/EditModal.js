import React, { Component } from 'react';
import {
  toggleEditModal,
  editMessage,
  dropCurrentMessageId,
} from '../../actions';
import { connect } from 'react-redux';
import './EditModal.scss';

class EditModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      inputValue: '',
    };

    this.inputHandler = this.inputHandler.bind(this);
    this.onSave = this.onSave.bind(this);
    this.onClose = this.onClose.bind(this);
  }

  componentDidMount() {
    const { messages, currentMessageId } = this.props.chat;
    const currentMessage = messages.find(
      (message) => message.id === currentMessageId
    );
    this.setState({ inputValue: currentMessage.text });
  }

  onSave() {
    const { editMessage, dropCurrentMessageId, toggleEditModal } = this.props;
    const { messages, currentMessageId } = this.props.chat;

    if (currentMessageId) {
      const currentMessage = messages.filter(
        (message) => message.id === currentMessageId
      );
      this.setState({ inputValue: currentMessage.text });
      editMessage(this.props.chat.currentMessageId, this.state.inputValue);
    }
    dropCurrentMessageId();
    toggleEditModal();
    this.setState({ inputValue: '' });
  }

  onClose() {
    const { dropCurrentMessageId, toggleEditModal } = this.props;

    dropCurrentMessageId();
    toggleEditModal();
  }

  inputHandler(e) {
    const inputValue = e.target.value;
    this.setState({ inputValue });
  }

  render() {
    return (
      <div className="edit-message-modal modal-shown">
        <div className="edit-message-modal__container">
          <span className="edit-message-modal__title">Edit your message:</span>
          <input
            className="edit-message-input edit-message-modal__input"
            type="text"
            onChange={this.inputHandler}
            value={this.state.inputValue}
            autoFocus
          />
          <div className="edit-message-modal__buttons">
            <button className="edit-message-close" onClick={this.onClose}>
              Close
            </button>

            <button className="edit-message-button" onClick={this.onSave}>
              Save
            </button>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    chat: state.chat,
  };
};

const mapDispatchToProps = () => {
  return {
    toggleEditModal,
    dropCurrentMessageId,
    editMessage,
  };
};

export default connect(mapStateToProps, mapDispatchToProps())(EditModal);
