import React, { Component } from 'react';
import { formatDateMessage } from '../../helpers';
import './Message.scss';

class Message extends Component {
  constructor(props) {
    super(props);

    this.state = {
      liked: false,
    };

    this.likeHandler = this.likeHandler.bind(this);
  }

  likeHandler() {
    this.setState({ liked: !this.state.liked });
  }

  render() {
    const { message } = this.props;
    const { liked } = this.state;

    return (
      <div className="message">
        <img
          className="message-user-avatar"
          src={message.avatar}
          alt={message.user}
        />
        <div className="message-text-container">
          <span className="message-user-name">{message.user}</span>

          <div className="message-text-button">
            <span className="message-text">{message.text}</span>
            <button
              className={liked ? 'message-liked' : 'message-like'}
              onClick={this.likeHandler}
            >
              &#x2764;
            </button>
          </div>
          <span className="message-time">
            {formatDateMessage(message.createdAt)}
          </span>
        </div>
      </div>
    );
  }
}

export default Message;
